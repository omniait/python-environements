import music21 as m21

scores_path = "../../resources/mxl_scores/"

stream = m21.converter.parse(scores_path + 'brian-crain-dream-of-flying.mxl')

notes = stream.recurse().notes

for obj in notes:
    if type(obj).__name__ == 'Note':
        note = obj
        print(note.nameWithOctave, "_",
              note.duration.quarterLength)
    elif type(obj).__name__ == 'Chord':
        chord = obj
        for note in chord:
            print(note.nameWithOctave)
        print(chord.duration.quarterLength)
        break
    else:
        print("Not Chord and Not Note")
        break     

print(stream.duration.quarterLength)

stream.write('midi', fp = scores_path)